package litowinski.gracjan.avengatask.transport;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.logging.Logger;

@RestController
@AllArgsConstructor
public class TransportController {

    private static final Logger LOGGER = Logger.getLogger(TransportService.class.getName());
    private final TransportService transportService;

    @GetMapping("/shipping-amount")
    public ResponseEntity<String> getShippingAmount() throws FileNotFoundException {
        LOGGER.info("Creating response with calculated shipping amount");
        String responseMessage = "Minimum times car must be used to provide all packages - "
                + transportService.getShippingAmount();
        return ResponseEntity.ok().body(responseMessage);
    }

    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<Object> handleException(FileNotFoundException e) {
        LOGGER.severe("There was error fetching data file");
        String message = "File with data is not provided or has wrong path.\nPlease check again.";
        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }
}
