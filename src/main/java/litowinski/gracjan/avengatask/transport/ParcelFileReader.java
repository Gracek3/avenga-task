package litowinski.gracjan.avengatask.transport;

import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@Component
public class ParcelFileReader {
    private final String fileName = "Data.csv";

    public List<Parcel> mapFileDataToParcelObject() throws FileNotFoundException {
        return new CsvToBeanBuilder<Parcel>(new FileReader(fileName))
                .withSeparator(';')
                .withType(Parcel.class)
                .withSkipLines(1)
                .build()
                .parse();
    }
}
