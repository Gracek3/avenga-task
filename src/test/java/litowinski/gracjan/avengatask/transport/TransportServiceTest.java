package litowinski.gracjan.avengatask.transport;

import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TransportServiceTest {

    static TransportService transportService;
    static ParcelFileReader parcelFileReader;

    @BeforeAll
    static void setUp() {
        parcelFileReader = mock(ParcelFileReader.class);
        transportService = new TransportService(parcelFileReader);
    }

    @Test
    void checkCalculationsOfShippingAmountWithTooBigParcel() throws FileNotFoundException {
        //given + when
        when(parcelFileReader.mapFileDataToParcelObject())
                .thenReturn(Collections.singletonList(new Parcel("example", 260)));
        int shippingAmount = transportService.getShippingAmount();

        //then
        Assertions.assertThat(shippingAmount).isEqualTo(0);
    }

    @Test
    void checkCalculationsOfShippingAmountWithNoParcels() throws FileNotFoundException {
        //given + when
        when(parcelFileReader.mapFileDataToParcelObject())
                .thenReturn(Collections.emptyList());
        int shippingAmount = transportService.getShippingAmount();

        //then
        Assertions.assertThat(shippingAmount).isEqualTo(0);
    }

    @Test
    void checkCalculationsOfShippingAmountWithCorrectParcels() throws FileNotFoundException {
        //given
        List<Parcel> parcels = generateNewParcels(5);

        // when
        when(parcelFileReader.mapFileDataToParcelObject())
                .thenReturn(parcels);
        int shippingAmount = transportService.getShippingAmount();

        //then
        Assertions.assertThat(shippingAmount).isNotEqualTo(0);
    }

    @Test
    void throwExceptionWhenFileUnavailable() throws FileNotFoundException {
        //given + when
        when(parcelFileReader.mapFileDataToParcelObject()).thenThrow(FileNotFoundException.class);

        assertThrows(FileNotFoundException.class, () -> parcelFileReader.mapFileDataToParcelObject());
    }

    private List<Parcel> generateNewParcels(int amount) {
        List<Parcel> parcels = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            String randomParcelNr = RandomStringUtils.randomAlphabetic(5);
            int randomParcelAmount = new Random().nextInt(250) + 1;
            parcels.add(new Parcel(randomParcelNr, randomParcelAmount));
        }
        return parcels;
    }
}