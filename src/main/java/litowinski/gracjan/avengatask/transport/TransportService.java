package litowinski.gracjan.avengatask.transport;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TransportService {

    private final ParcelFileReader parcelFileReader;

    public int getShippingAmount() throws FileNotFoundException {
        return calculateShippingAmount();
    }

    private int calculateShippingAmount() throws FileNotFoundException {
        int truckCapacity = 250;

        List<Parcel> parcels = parcelFileReader.mapFileDataToParcelObject().stream()
                .filter(parcel -> parcel.getAmount() <= truckCapacity)
                .sorted(Comparator.comparing(Parcel::getAmount).reversed())
                .collect(Collectors.toList());

        int shippingAmount = 0;

        Parcel[] remainingSpace = new Parcel[parcels.size()];

        for (Parcel parcel : parcels) {
            int j;
            for (j = 0; j < shippingAmount; j++) {
                if (remainingSpace[j].getAmount() >= parcel.getAmount()) {
                    remainingSpace[j] = new Parcel(parcel.getPackageNr(),
                            remainingSpace[j].getAmount() - parcel.getAmount());
                    break;
                }
            }

            if (j == shippingAmount) {
                remainingSpace[shippingAmount] = new Parcel(parcel.getPackageNr(),
                        truckCapacity - parcel.getAmount());
                shippingAmount++;
            }
        }

        return shippingAmount;
    }
}
