package litowinski.gracjan.avengatask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvengaTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvengaTaskApplication.class, args);
	}

}
