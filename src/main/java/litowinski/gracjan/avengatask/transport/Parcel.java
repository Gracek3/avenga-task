package litowinski.gracjan.avengatask.transport;

import com.opencsv.bean.CsvBindByPosition;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Parcel {

    @CsvBindByPosition(position = 0)
    private String packageNr;
    @CsvBindByPosition(position = 1)
    private int amount;
}
