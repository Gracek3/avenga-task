Project has build-in tomcat.
To run server, please use <code>mvn spring-boot:run</code>

To check solution, please use webbrowser or Postman(GET method) and go to <a>localhost:8080/shipping-amount</a>